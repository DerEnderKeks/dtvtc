# Discord Temporary Voice Text Channel

This bot adds Teamspeak like text channels that are bound to a voice channel to Discord. 
Each text channel is only accessible while the user is in the corresponding voice channel. 
The channels are created and deleted when users join/leave voice channels.

## Commands

- `/info` - show information about this bot
- `/settings <show|set <setting> <value>>` - Show or change settings; Requires Manage Server permission
- `/unlink [voice channel]` - Unlink the text channel from the voice channel; Requires Manage Channels permission

## Settings

You can tab complete the setting names.

- `Channel Name Template` - Naming format for newly created text channels, use `%vc_name%` as a placeholder for the voice channel name.
- `Disable History` - Revoke the channel history permission in temporary text channels. Value starting with `y`, `t` or `1` are truthy, anything else is false.

## Adding the bot to a server

The bot requires access to the server members intent (set in the Bot settings in the Discord developer dashboard).

Required Scopes: `bot`, `applications.commands`

Permissions:
- create channel
- manage roles
- view channels

Bits: `268435472`

Join the bot using this link (insert your bot id): `https://discord.com/api/oauth2/authorize?client_id=%s&permissions=268435472&scope=bot%20applications.commands`

## Installation

### Config

The following environment variables are used to configure the bot:

- `BOT_TOKEN` - self-explanatory
- `BOT_DB_PATH` - path to store DB file at (not a directory), defaults to `./data.db`

### Docker

Image: `derenderkeks/dtvtc`

By default, the DB is stored at `/data/data.db`.

### Direct

This is a very basic Golang app, if you want to use it without Docker just follow the steps in the [Dockerfile](Dockerfile).

## License 

This project is licensed under [AGPL-3.0-or-later](License).
