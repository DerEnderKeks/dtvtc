package main

import (
	"gopkg.in/guregu/null.v4"
	"time"
)

type IDModel struct {
	ID        string    `gorm:"primaryKey"`
	CreatedAt time.Time ``
	UpdatedAt time.Time ``
}

type Server struct {
	IDModel
	BotRoleID           string      `gorm:"not null"`
	ChannelNameTemplate null.String ``
	DisableHistory      bool        `gorm:"default:false"`
	Channels            []Channel
}

type Channel struct {
	IDModel
	ServerID      string ``
	Server        Server `gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	TextChannelID string `gorm:"uniqueIndex"`
	RoleID        string `gorm:"uniqueIndex"`
}
