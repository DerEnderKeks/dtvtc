module gitlab.com/DerEnderKeks/dtvtc

go 1.17

require (
	github.com/bwmarrin/discordgo v0.23.3-0.20211228023845-29269347e820
	github.com/spf13/jwalterweatherman v1.1.0
	gopkg.in/guregu/null.v4 v4.0.0
	gorm.io/driver/sqlite v1.2.6
	gorm.io/gorm v1.22.4
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/mattn/go-sqlite3 v1.14.9 // indirect
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b // indirect
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68 // indirect
)
