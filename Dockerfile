FROM golang:alpine as builder

RUN apk add --update --no-cache \
    gcc musl-dev \
    tzdata \
    upx

WORKDIR /app

COPY . .
RUN CGO_ENABLED=1 go build --tags="linux" -ldflags="-w -s" -o app
RUN upx app

FROM alpine:latest
LABEL maintainer="DerEnderKeks"

RUN addgroup -g 1000 -S app \
    && adduser -u 1000 -S -G app app
RUN mkdir /data && chown -R app:app /data

COPY --from=builder /app/app /app

VOLUME /data

USER app

ENV BOT_DB_PATH /data/data.db
CMD ["/app"]