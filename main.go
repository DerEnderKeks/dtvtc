package main

import (
	"errors"
	"fmt"
	"github.com/bwmarrin/discordgo"
	jww "github.com/spf13/jwalterweatherman"
	"gopkg.in/guregu/null.v4"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/logger"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

func main() {
	jww.SetStdoutThreshold(jww.LevelInfo)

	discordToken := os.Getenv("BOT_TOKEN")
	if len(discordToken) == 0 {
		jww.FATAL.Fatalln("BOT_TOKEN is not set.")
	}

	dbPath := getEnvOrDefault("BOT_DB_PATH", "data.db")

	db := initDB(dbPath)
	startBot(discordToken, db)
}

func initDB(path string) *gorm.DB {
	newLogger := logger.New(
		jww.LOG, // io writer
		logger.Config{
			SlowThreshold:             200 * time.Millisecond,
			LogLevel:                  logger.Warn,
			IgnoreRecordNotFoundError: true,
			Colorful:                  true,
		},
	)
	db, err := gorm.Open(sqlite.Open(path), &gorm.Config{
		Logger: newLogger,
	})

	rawDB, err := db.DB()
	if err != nil {
		jww.FATAL.Fatalln(fmt.Sprintf("Failed to get DB object: %v", err))
	}

	rawDB.SetMaxOpenConns(1) // prevent accessing the db while it's locked from another goroutine

	if err != nil {
		jww.FATAL.Fatalln(fmt.Sprintf("Failed to open db: %v", err))
	}

	err = db.AutoMigrate(&Server{}, &Channel{})
	if err != nil {
		jww.FATAL.Fatalln(fmt.Sprintf("Failed to auto-migrate db: %v", err))
	}

	return db
}

type Command struct {
	description string
	options     []*discordgo.ApplicationCommandOption
	handler     func(s *discordgo.Session, event *discordgo.InteractionCreate)
}

type Bot struct {
	session *discordgo.Session
	db      *gorm.DB

	applicationID string

	commands map[string]Command
}

func startBot(token string, db *gorm.DB) {
	discord, err := discordgo.New("Bot " + token)
	if err != nil {
		jww.FATAL.Fatalln(fmt.Sprintf("Failed to init discordgo: %v", err))
	}

	application, err := discord.Application("@me")
	if err != nil {
		jww.ERROR.Fatalln(fmt.Sprintf("Failed to get application: %v", err))
	}

	jww.INFO.Printf("Invite URL: https://discord.com/api/oauth2/authorize?client_id=%s&permissions=268435472&scope=bot%%20applications.commands", application.ID)

	bot := &Bot{
		session:       discord,
		db:            db,
		applicationID: application.ID,
	}
	bot.createCommandHandlers()

	discord.Identify.Intents = discordgo.IntentsGuilds | discordgo.IntentsGuildVoiceStates | discordgo.IntentsGuildMembers

	bot.setupHandlers()

	jww.INFO.Println("Connecting to Discord...")
	err = discord.Open()
	if err != nil {
		jww.FATAL.Fatalln(fmt.Sprintf("Failed to open Discord session: %v", err))
	}

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	jww.INFO.Println("Shutting down...")
	_ = discord.Close()
}

func (bot *Bot) createCommandHandlers() {
	bot.commands = map[string]Command{
		"info": {
			description: "Get information about this bot",
			handler: func(s *discordgo.Session, event *discordgo.InteractionCreate) {
				err := s.InteractionRespond(event.Interaction, &discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,
					Data: &discordgo.InteractionResponseData{
						Content: "[DTVTC](https://gitlab.com/DerEnderKeks/dtvtc) by [DerEnderKeks](https://derenderkeks.me)" +
							"\n\nAutomatically creates temporary text channels for voice channels.",
						Flags: 1 << 6,
					},
				})
				if err != nil {
					jww.ERROR.Printf("Failed to respond to /info command: %v", err)
				}
			},
		},
		"settings": {
			description: "View or change settings for this bot",
			options: []*discordgo.ApplicationCommandOption{
				{
					Name:        "show",
					Description: "Lists the current settings",
					Type:        discordgo.ApplicationCommandOptionSubCommand,
				},
				{
					Name:        "set",
					Description: "Set a setting to new value",
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Options: []*discordgo.ApplicationCommandOption{
						{
							Type:        discordgo.ApplicationCommandOptionString,
							Name:        "setting",
							Description: "The setting to change",
							Required:    true,
							Choices: []*discordgo.ApplicationCommandOptionChoice{
								{
									Name:  "Disable History",
									Value: "disable_history",
								},
								{
									Name:  "Channel Name Template",
									Value: "channel_name_template",
								},
							},
						},
						{
							Type:        discordgo.ApplicationCommandOptionString,
							Name:        "value",
							Description: "The new value for the setting",
							Required:    true,
						},
					},
				},
			},
			handler: func(s *discordgo.Session, event *discordgo.InteractionCreate) {
				if event.Member.Permissions&discordgo.PermissionManageServer == 0 {
					err := bot.sendInteractionResponse(event.Interaction, "You don't have permission to use this command.")
					if err != nil {
						jww.ERROR.Printf("Failed to respond to /settings command: %v", err)
					}
					return
				}

				subCmd := event.ApplicationCommandData().Options[0].Name

				server := &Server{
					IDModel: IDModel{
						ID: event.GuildID,
					},
				}
				err := bot.db.First(server, server).Error
				if err != nil {
					jww.ERROR.Printf("Failed to find server to get settings: %s: %v", event.GuildID, err)
					return
				}

				switch subCmd {
				case "show":
					channelNameTemplate := defaultChannelNameTemplate
					if server.ChannelNameTemplate.Valid {
						channelNameTemplate = server.ChannelNameTemplate.String
					}

					response := fmt.Sprintf(":gear: Settings for this server:\n\n"+
						"Channel Name Template: `%s`\n"+
						"Disable History: `%s`",
						channelNameTemplate, ternary(server.DisableHistory, "✅", "❌"))
					err = bot.sendInteractionResponse(event.Interaction, response)
				case "set":
					setting := event.ApplicationCommandData().Options[0].Options[0].StringValue()
					value := event.ApplicationCommandData().Options[0].Options[1].StringValue()

					switch setting {
					case "channel_name_template":
						server.ChannelNameTemplate = null.StringFrom(value)
						err = bot.db.Save(server).Error
						if err != nil {
							jww.ERROR.Printf("Failed to save setting: %s: %s: %v", event.GuildID, setting, err)
							return
						}
						err = bot.sendInteractionResponse(event.Interaction, "Setting saved.")
					case "disable_history":
						newValue := func() bool {
							trueValues := []string{"t", "y", "1"}
							for _, trueValue := range trueValues {
								if strings.HasPrefix(value, trueValue) {
									return true
								}
							}
							return false
						}()
						server.DisableHistory = newValue
						err = bot.db.Save(server).Error
						if err != nil {
							jww.ERROR.Printf("Failed to save setting: %s: %s: %v", event.GuildID, setting, err)
							return
						}
						err = bot.sendInteractionResponse(event.Interaction, "Setting saved.")
					default:
						err = bot.sendInteractionResponse(event.Interaction, "Invalid setting.")
						break
					}
				}
				if err != nil {
					jww.ERROR.Printf("Failed to respond to /settings command: %v", err)
				}
			},
		},
		"unlink": {
			description: "Unlink a text channel from a voice channel (uses the one you're currently in unless specified)",
			options: []*discordgo.ApplicationCommandOption{
				{
					Type:         discordgo.ApplicationCommandOptionChannel,
					Name:         "channel",
					Description:  "Voice channel to unlink",
					ChannelTypes: []discordgo.ChannelType{discordgo.ChannelTypeGuildVoice},
					Required:     false,
				},
			},
			handler: func(s *discordgo.Session, event *discordgo.InteractionCreate) {
				if event.Member.Permissions&discordgo.PermissionManageChannels == 0 {
					err := bot.sendInteractionResponse(event.Interaction, "You don't have permission to use this command.")
					if err != nil {
						jww.ERROR.Printf("Failed to respond to /unlink command: %v", err)
					}
					return
				}

				var channelID string
				if len(event.ApplicationCommandData().Options) > 0 {
					channelValue := event.ApplicationCommandData().Options[0].ChannelValue(nil)
					channelID = channelValue.ID
				} else {
					if event.Member == nil || event.Member.User == nil {
						jww.ERROR.Printf("User is nil: /unlink")
						return
					}

					voiceChannelID, err := bot.userVoiceChannel(event.GuildID, event.Member.User.ID)
					if err != nil {
						jww.ERROR.Printf("Failed to get voice channel of user: %v", err)
						return
					}

					if len(voiceChannelID) == 0 {
						err = bot.sendInteractionResponse(event.Interaction, "You are not in a voice channel.")
						if err != nil {
							jww.ERROR.Printf("Failed to respond to /unlink command: %v", err)
						}
						return
					}
					channelID = voiceChannelID
				}

				dbChannel := &Channel{IDModel: IDModel{ID: channelID}}
				err := bot.db.First(dbChannel, dbChannel).Error
				if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
					jww.ERROR.Printf("Failed to get channel from DB: %s: %v", channelID, err)
					return
				} else if errors.Is(err, gorm.ErrRecordNotFound) {
					err = bot.sendInteractionResponse(event.Interaction, "No linked channel found.")
					if err != nil {
						jww.ERROR.Printf("Failed to respond to /unlink command: %v", err)
					}
					return
				}

				err = bot.db.Delete(dbChannel).Error
				if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
					jww.ERROR.Printf("Failed to delete channel from DB: %s: %v", channelID, err)
				}

				unlinkedCharacter := "🔹"

				textChannel, err := bot.session.Channel(dbChannel.TextChannelID)
				if err != nil {
					jww.ERROR.Printf("Failed to query text channel: %s: %v", dbChannel.TextChannelID, err)
					return
				}
				_, err = bot.session.ChannelEdit(textChannel.ID, textChannel.Name+unlinkedCharacter)
				if err != nil {
					jww.ERROR.Printf("Failed to rename text channel: %s: %v", dbChannel.TextChannelID, err)
					return
				}

				roles, err := bot.session.GuildRoles(event.GuildID)
				if err != nil {
					jww.ERROR.Printf("Failed to query guild roles: %s: %v", event.GuildID, err)
					return
				}

				var role *discordgo.Role
				for _, r := range roles {
					if r.ID == dbChannel.RoleID {
						role = r
					}
				}
				if role == nil {
					jww.ERROR.Printf("Failed to find text channel role: %s: %v", dbChannel.RoleID, err)
					return
				}

				_, err = bot.session.GuildRoleEdit(event.GuildID, role.ID, role.Name+unlinkedCharacter, 0, false, 0, false)
				if err != nil {
					jww.ERROR.Printf("Failed to rename text channel role: %s: %v", role.ID, err)
					return
				}

				err = bot.sendInteractionResponse(event.Interaction, "Successfully unlinked text channel "+textChannel.Mention())
				if err != nil {
					jww.ERROR.Printf("Failed to respond to /unlink command: %v", err)
				}
			},
		},
	}
}

func (bot *Bot) userVoiceChannel(guildID, userID string) (string, error) {
	guild, err := bot.session.State.Guild(guildID)
	if err != nil {
		return "", fmt.Errorf("failed to query guild: %s: %v", guildID, err)
	}

	for _, state := range guild.VoiceStates {
		if state.UserID == userID {
			return state.ChannelID, nil
		}
	}

	return "", nil
}

func (bot *Bot) sendInteractionResponse(interaction *discordgo.Interaction, message string) error {
	return bot.session.InteractionRespond(interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: message,
			Flags:   1 << 6,
		},
	})
}

func (bot *Bot) setupHandlers() {
	jww.INFO.Println("Registering event handlers...")
	addHandler := bot.session.AddHandler

	addHandler(bot.readyHandler)
	addHandler(bot.commandHandler)
	addHandler(bot.voiceStateHandler)
	addHandler(bot.guildCreateHandler)
	addHandler(bot.guildDeleteHandler)
	addHandler(bot.channelDeletedHandler)
	addHandler(bot.roleDeletedHandler)
}

func (bot *Bot) readyHandler(s *discordgo.Session, event *discordgo.Ready) {
	err := s.UpdateStatusComplex(discordgo.UpdateStatusData{
		Activities: []*discordgo.Activity{{
			Name: "your voice channels.",
			Type: discordgo.ActivityTypeWatching,
		}},
		Status: "online",
	})
	if err != nil {
		jww.ERROR.Printf("Failed to set Discord status: %v", err)
	}

	jww.INFO.Println("Bot connected.")
	bot.syncStoredData()
}

func (bot *Bot) syncStoredData() {
	jww.INFO.Println("Syncing stored data...")

	var servers []Server
	err := bot.db.Find(&servers).Error
	if err != nil {
		jww.ERROR.Fatalf("Failed to get all servers from DB: %v", err)
	}

	var botGuildIDs []string
	for {
		afterID := ""
		if len(botGuildIDs) > 0 {
			afterID = botGuildIDs[len(botGuildIDs)-1]
		}

		guildsRes, err := bot.session.UserGuilds(0, "", afterID)
		if err != nil {
			jww.ERROR.Fatalf("Failed to query all guilds: %v", err)
		}

		if len(guildsRes) == 0 {
			break
		}
		for _, userGuild := range guildsRes {
			botGuildIDs = append(botGuildIDs, userGuild.ID)
		}
	}

	// Cleanup stale servers and channels
	for _, server := range servers {
		func() {
			for _, guildID := range botGuildIDs {
				if guildID != server.ID {
					continue
				}

				var channels []Channel
				err = bot.db.Where(&Channel{ServerID: server.ID}).Find(&channels).Error
				if err != nil {
					jww.ERROR.Printf("Failed to get all channels for server from DB: %s: %v", server.ID, err)
					return
				}

				guildChannels, err := bot.session.GuildChannels(server.ID)
				if err != nil {
					jww.ERROR.Printf("Failed to query channels for guild: %s: %v", server.ID, err)
					return
				}

				guildRoles, err := bot.session.GuildRoles(server.ID)
				if err != nil {
					jww.ERROR.Printf("Failed to query roles for guild: %s: %v", server.ID, err)
					return
				}

				guild, err := bot.session.State.Guild(guildID)
				if err != nil {
					jww.ERROR.Printf("Failed to get guild state: %s: %v", guildID, err)
					return
				}

				populatedVoiceChannels := make(map[string]struct{})
				for _, voiceState := range guild.VoiceStates {
					populatedVoiceChannels[voiceState.ChannelID] = struct{}{}
				}

				for _, channel := range channels {
					func() {
						for _, guildChannel := range guildChannels {
							if guildChannel.Type == discordgo.ChannelTypeGuildVoice && channel.ID == guildChannel.ID {
								if _, ok := populatedVoiceChannels[channel.ID]; !ok {
									break // channel is empty, cleanup
								}
								for _, c := range guildChannels {
									if c.Type == discordgo.ChannelTypeGuildText && c.ID == channel.TextChannelID {
										for _, guildRole := range guildRoles {
											if guildRole.ID == channel.RoleID {
												return // voice channel, linked text channel and role exist
											}
										}
									}
								}
							}
						}
						jww.INFO.Printf("Cleaning up stale channel: %s", channel.ID)
						bot.cleanupChannelPair(channel.ID)
					}()
				}

				checkedChannels := make(map[string]struct{})
				for _, voiceState := range guild.VoiceStates {
					if _, ok := checkedChannels[voiceState.ChannelID]; ok {
						continue
					}
					checkedChannels[voiceState.ChannelID] = struct{}{}
					func() {
						for _, channel := range channels {
							if channel.ID == voiceState.ChannelID {
								return
							}
						}

						jww.INFO.Printf("Creating missing channel: %s", voiceState.ChannelID)
						err = bot.ensureChannelPairExists(guildID, voiceState.ChannelID)
						if err != nil {
							jww.ERROR.Printf("Failed to ensure channel pair exists: %s: %v", voiceState.ChannelID, err)
							return
						}

						rolesAdded := 0
						for _, state := range guild.VoiceStates {
							if state.ChannelID != voiceState.ChannelID {
								continue
							}
							err = bot.ensureUserHasTextChannelRole(voiceState.ChannelID, voiceState.UserID)
							if err != nil {
								jww.ERROR.Printf("Failed to add user to channel role: %s: %s: %v", voiceState.UserID, voiceState.ChannelID, err)
							} else {
								rolesAdded++
							}
						}
						jww.INFO.Printf("Added %d users to channel: %s", rolesAdded, voiceState.ChannelID)
					}()
				}

				return
			}
			err = bot.db.Delete(&server).Error
			if err != nil {
				jww.ERROR.Printf("Failed to delete stale server: %s: %v", server.ID, err)
			} else {
				jww.INFO.Printf("Deleted stale server: %s", server.ID)
			}
		}()
	}
}

func (bot *Bot) commandHandler(s *discordgo.Session, event *discordgo.InteractionCreate) {
	cmdName := event.ApplicationCommandData().Name

	if cmd, ok := bot.commands[cmdName]; ok {
		cmd.handler(s, event)
	} else {
		jww.WARN.Printf("Invalid command was requested: %v", cmd)
	}
}

func (bot *Bot) voiceStateHandler(s *discordgo.Session, event *discordgo.VoiceStateUpdate) {
	if event.BeforeUpdate != nil && event.ChannelID == event.BeforeUpdate.ChannelID {
		return // ignore when not switching channels
	}

	if event.BeforeUpdate != nil && event.BeforeUpdate.ChannelID != "" {
		err := bot.removeFromTextChannelRole(event.BeforeUpdate.ChannelID, event.BeforeUpdate.UserID)
		if err != nil {
			jww.ERROR.Printf("Failed to remove role from user: %s: %v", event.BeforeUpdate.ChannelID, err)
		}

		channelUsers, err := bot.voiceChannelUsers(event.BeforeUpdate.GuildID, event.BeforeUpdate.ChannelID)
		if err != nil {
			jww.ERROR.Printf("Failed to get voice channel users: %s: %v", event.BeforeUpdate.ChannelID, err)
		} else {
			if len(channelUsers) == 0 {
				bot.cleanupChannelPair(event.BeforeUpdate.ChannelID)
			}
		}
	}
	if event.ChannelID != "" {
		err := bot.ensureChannelPairExists(event.GuildID, event.ChannelID)
		if err != nil {
			jww.ERROR.Printf("Failed to ensure text channel existence: %s: %v", event.ChannelID, err)
		} else {
			err = bot.ensureUserHasTextChannelRole(event.ChannelID, event.UserID)
			if err != nil {
				jww.ERROR.Printf("Failed to ensure that user has channel role: %s: %s: %v", event.UserID, event.ChannelID, err)
			}
		}
	}
}

func (bot *Bot) voiceChannelUsers(guildID, channelID string) ([]string, error) {
	guild, err := bot.session.State.Guild(guildID)
	if err != nil {
		return nil, fmt.Errorf("failed to get guild: %s: %v", guildID, err)
	}

	var users []string
	for _, voiceState := range guild.VoiceStates {
		if voiceState.ChannelID == channelID {
			users = append(users, voiceState.UserID)
		}
	}
	return users, nil
}

func (bot *Bot) cleanupChannelPair(channelID string) {
	dbChannel := &Channel{IDModel: IDModel{ID: channelID}}
	err := bot.db.First(dbChannel, dbChannel).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		jww.ERROR.Printf("Failed to get channel from DB: %s: %v", channelID, err)
		return
	} else if errors.Is(err, gorm.ErrRecordNotFound) {
		return
	}

	err = bot.db.Delete(dbChannel).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		jww.ERROR.Printf("Failed to delete channel from DB: %s: %v", channelID, err)
	}

	err = bot.session.GuildRoleDelete(dbChannel.ServerID, dbChannel.RoleID)
	if err != nil {
		restErr, ok := err.(discordgo.RESTError)
		if !ok || restErr.Response.StatusCode != http.StatusNotFound {
			jww.ERROR.Printf("Failed to delete channel role: %s: %s: %v", channelID, dbChannel.RoleID, err)
		}
	}

	_, err = bot.session.ChannelDelete(dbChannel.TextChannelID)
	if err != nil {
		restErr, ok := err.(discordgo.RESTError)
		if !ok || restErr.Response.StatusCode != http.StatusNotFound {
			jww.ERROR.Printf("Failed to delete channel: %s: %v", channelID, err)
		}
	}
}

func (bot *Bot) removeFromTextChannelRole(channelID, userID string) error {
	dbChannel := &Channel{IDModel: IDModel{ID: channelID}}
	err := bot.db.First(dbChannel, dbChannel).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return fmt.Errorf("failed to get channel from DB: %s: %v", channelID, err)
	} else if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil
	}

	err = bot.session.GuildMemberRoleRemove(dbChannel.ServerID, userID, dbChannel.RoleID)
	if err != nil {
		return fmt.Errorf("failed to remove role from user: %s: %s: %v", dbChannel.RoleID, userID, err)
	}

	return nil
}

func (bot *Bot) ensureChannelPairExists(guildID, channelID string) error {
	tx := bot.db.Begin()

	dbChannel := &Channel{IDModel: IDModel{ID: channelID}, ServerID: guildID}
	err := tx.First(dbChannel, dbChannel).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return fmt.Errorf("failed to find channel in DB: %s: %v", channelID, err)
	} else if err == nil {
		return nil
	}

	err = tx.Preload(clause.Associations).Create(dbChannel).Error
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("failed to save channel in DB: %s: %v", channelID, err)
	}

	dbServer := &Server{IDModel: IDModel{ID: guildID}}
	err = tx.First(dbServer, dbServer).Error
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("failed to get server from DB: %s: %v", guildID, err)
	}

	voiceChannel, err := bot.session.Channel(channelID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("failed to query voice channel: %s: %v", channelID, err)
	}

	role, err := bot.createRole(guildID, channelID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("failed to create role: %s: %v", channelID, err)
	}

	textChannel, err := bot.createTextChannel(guildID, channelID, dbServer, role.ID, voiceChannel, err)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("failed to create text channel: %s: %v", channelID, err)
	}

	dbChannel.TextChannelID = textChannel.ID
	dbChannel.RoleID = role.ID
	err = tx.Save(dbChannel).Error
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("failed to save channel data to DB: %s: %v", channelID, err)
	}

	err = tx.Commit().Error
	if err != nil {
		return fmt.Errorf("failed to commit channel data to DB: %s: %v", channelID, err)
	}
	return nil
}

func (bot *Bot) createTextChannel(guildID string, channelID string, dbServer *Server, roleID string, voiceChannel *discordgo.Channel, err error) (*discordgo.Channel, error) {
	allowPermissions := int64(discordgo.PermissionViewChannel)
	denyPermissions := int64(discordgo.PermissionViewChannel)
	if dbServer.DisableHistory {
		denyPermissions |= discordgo.PermissionReadMessageHistory
	}

	permissionOverwrites := []*discordgo.PermissionOverwrite{
		{
			ID:    dbServer.BotRoleID, // bot role
			Type:  discordgo.PermissionOverwriteTypeRole,
			Deny:  0,
			Allow: allowPermissions,
		},
		{
			ID:    roleID, // channel role
			Type:  discordgo.PermissionOverwriteTypeRole,
			Deny:  0,
			Allow: allowPermissions,
		},
		{
			ID:    guildID, // @everyone role
			Type:  discordgo.PermissionOverwriteTypeRole,
			Deny:  denyPermissions,
			Allow: 0,
		},
	}

	nameTemplate := ternary(dbServer.ChannelNameTemplate.Valid, dbServer.ChannelNameTemplate.String, defaultChannelNameTemplate)
	channelName := strings.ReplaceAll(nameTemplate, "%vc_name%", voiceChannel.Name)

	newTextChannel, err := bot.session.GuildChannelCreateComplex(guildID, discordgo.GuildChannelCreateData{
		Name:                 channelName,
		Type:                 discordgo.ChannelTypeGuildText,
		PermissionOverwrites: permissionOverwrites,
		ParentID:             voiceChannel.ParentID,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to create text channel: %s: %v", channelID, err)
	}
	return newTextChannel, nil
}

func (bot *Bot) createRole(guildID, channelID string) (*discordgo.Role, error) {
	role, err := bot.session.GuildRoleCreate(guildID)
	if err != nil {
		return nil, fmt.Errorf("failed to create role: %s: %v", channelID, err)
	}

	role, err = bot.session.GuildRoleEdit(guildID, role.ID, "tvtc-"+channelID, 0, false, 0, false)
	if err != nil {
		return nil, fmt.Errorf("failed to edit role: %s: %v", channelID, err)
	}
	return role, nil
}

func (bot *Bot) ensureUserHasTextChannelRole(channelID, userID string) error {
	dbChannel := &Channel{IDModel: IDModel{ID: channelID}}
	err := bot.db.First(dbChannel, dbChannel).Error
	if err != nil {
		return fmt.Errorf("failed to get channel from DB: %s: %v", channelID, err)
	}

	err = bot.session.GuildMemberRoleAdd(dbChannel.ServerID, userID, dbChannel.RoleID)
	if err != nil {
		return fmt.Errorf("failed to assign role to user: %s: %s: %v", dbChannel.RoleID, userID, err)
	}

	return nil
}

func (bot *Bot) channelDeletedHandler(s *discordgo.Session, event *discordgo.ChannelDelete) {
	if event.Type != discordgo.ChannelTypeGuildText {
		return
	}

	dbChannel := &Channel{TextChannelID: event.ID}
	err := bot.db.First(dbChannel, dbChannel).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		jww.ERROR.Printf("failed to find text channel in DB: %s: %v", event.ID, err)
		return
	} else if errors.Is(err, gorm.ErrRecordNotFound) {
		return
	}

	err = bot.session.GuildRoleDelete(dbChannel.ServerID, dbChannel.RoleID)
	if err != nil {
		restErr, ok := err.(discordgo.RESTError)
		if !ok || restErr.Response.StatusCode != http.StatusNotFound {
			jww.ERROR.Printf("Failed to delete channel role: %s: %s: %v", dbChannel.ID, dbChannel.RoleID, err)
		}
	}

	err = bot.db.Delete(dbChannel).Error
	if err != nil {
		jww.ERROR.Printf("Failed to delete channel from DB: %s: %v", dbChannel.ID, err)
	}
}

func (bot *Bot) roleDeletedHandler(s *discordgo.Session, event *discordgo.GuildRoleDelete) {
	dbChannel := &Channel{RoleID: event.RoleID}
	err := bot.db.First(dbChannel, dbChannel).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		jww.ERROR.Printf("failed to find role in DB: %s: %v", event.RoleID, err)
		return
	} else if errors.Is(err, gorm.ErrRecordNotFound) {
		return
	}

	_, err = bot.session.ChannelDelete(dbChannel.TextChannelID)
	if err != nil {
		restErr, ok := err.(discordgo.RESTError)
		if !ok || restErr.Response.StatusCode != http.StatusNotFound {
			jww.ERROR.Printf("Failed to delete text channel: %s: %s: %v", dbChannel.ID, dbChannel.RoleID, err)
		}
	}

	err = bot.db.Delete(dbChannel).Error
	if err != nil {
		jww.ERROR.Printf("Failed to delete channel from DB: %s: %v", dbChannel.ID, err)
	}
}

func (bot *Bot) guildCreateHandler(s *discordgo.Session, event *discordgo.GuildCreate) {
	bot.updateGuildCommands(event.ID)
	bot.registerServer(event.ID)
}

func (bot *Bot) registerServer(guildID string) {
	botMember, err := bot.session.GuildMember(guildID, bot.applicationID)
	if err != nil {
		jww.ERROR.Printf("Failed to find bot member: %s: %v", guildID, err)
	}
	guildRoles, err2 := bot.session.GuildRoles(guildID)
	if err != nil {
		jww.ERROR.Printf("Failed get guild roles: %s: %v", guildID, err)
	}

	leaveGuild := func() {
		err := bot.session.GuildLeave(guildID)
		if err != nil {
			jww.ERROR.Printf("Failed to leave guild: %s: %v", guildID, err)
		}
	}

	if err != nil || err2 != nil {
		leaveGuild()
		return
	}

	botRoleID := func() string {
		for _, roleID := range botMember.Roles {
			for _, role := range guildRoles {
				if role.ID == roleID && role.Managed {
					return roleID
				}
			}
		}
		return ""
	}()

	if len(botRoleID) == 0 {
		jww.ERROR.Printf("Failed to find bot role: %s", guildID)
		leaveGuild()
		return
	}

	server := &Server{
		IDModel: IDModel{
			ID: guildID,
		},
		BotRoleID: botRoleID,
	}
	err = bot.db.Clauses(clause.OnConflict{
		Columns:   []clause.Column{{Name: "id"}},
		DoUpdates: clause.AssignmentColumns([]string{"bot_role_id"}),
	}).Create(server).Error
	if err != nil {
		jww.ERROR.Printf("Failed to upsert server in DB: %s: %v", guildID, err)
		leaveGuild()
	}
}

func (bot *Bot) guildDeleteHandler(s *discordgo.Session, event *discordgo.GuildDelete) {
	bot.deleteServer(event.ID)
}

func (bot *Bot) deleteServer(guildID string) {
	jww.INFO.Printf("Deleting server: %s", guildID)

	server := &Server{
		IDModel: IDModel{
			ID: guildID,
		},
	}
	err := bot.db.Delete(server).Error
	if err != nil {
		jww.ERROR.Printf("Failed to delete server: %s: %v", guildID, err)
	}
}

func (bot *Bot) updateGuildCommands(guildID string) {
	registeredCommands, err := bot.session.ApplicationCommands(bot.applicationID, guildID)
	if err != nil {
		jww.ERROR.Printf("Failed to get application commands: %v", err)
	}

	changedCommands := 0
	for cmdName, cmd := range bot.commands {
		// TODO skip unmodified commands?
		var oldCommand *discordgo.ApplicationCommand
		for _, registeredCommand := range registeredCommands {
			if registeredCommand.Name == cmdName {
				oldCommand = registeredCommand
				break
			}
		}

		newCommand, err := bot.session.ApplicationCommandCreate(bot.applicationID, guildID, &discordgo.ApplicationCommand{
			Type:        discordgo.ChatApplicationCommand,
			Name:        cmdName,
			Description: cmd.description,
			Options:     cmd.options,
		})
		if err != nil {
			jww.ERROR.Printf("Failed to create guild application command: %s: %s: %v", cmdName, guildID, err)
		} else {
			if oldCommand == nil || oldCommand.Version != newCommand.Version {
				changedCommands++
			}
		}
	}

	deletedCommands := 0
	for _, registeredCommand := range registeredCommands {
		if _, ok := bot.commands[registeredCommand.Name]; !ok {
			err := bot.session.ApplicationCommandDelete(bot.applicationID, guildID, registeredCommand.ID)
			if err != nil {
				jww.ERROR.Printf("Failed to deleted old application command: %s: %s: %v", registeredCommand.Name, guildID, err)
			} else {
				deletedCommands++
			}
		}
	}

	if changedCommands > 0 || deletedCommands > 0 {
		jww.INFO.Printf("Updated guild commands for guild %s: %d updated, %d deleted", guildID, changedCommands, deletedCommands)
	}
}

const defaultChannelNameTemplate = "🔊%vc_name%"

func getEnvOrDefault(key, def string) string {
	val, ok := os.LookupEnv(key)
	if !ok {
		return def
	}
	return val
}

func ternary(cond bool, a, b string) string {
	if cond {
		return a
	}
	return b
}
